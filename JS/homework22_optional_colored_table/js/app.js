const insertToHTML = (place, data) => place.insertAdjacentHTML('beforeend', data) || place.lastElementChild
const removeFromHTML = (parent, elem) => parent.removeChild(elem)

const createTable = (row, col) => {

    const createRow = row => {
        const td = `<td class="cell"></td>`
        let rows = ''
        for (let i = 0; i < row; i++) {
            rows += td
        }
        return rows
    }
    const createCol = col => {
        let cols = ''
        for (let i = 0; i < col; i++) {
            cols += `<tr class="col">${createRow(row)}</tr>`
        }
        return cols
    }
    return insertToHTML(document.body, `<table>${createCol(col)}</table>`)
}

const table = createTable(30, 30)

document.body.addEventListener('click', e => {
    if (e.target.classList.contains('cell')) {
        if (e.target.style.backgroundColor !== 'black') e.target.style.backgroundColor = 'black'
        else e.target.style.backgroundColor = 'white'
    } else {
        const cells = document.querySelectorAll('.cell')
        cells.forEach(cell => cell.style.backgroundColor = 'white')
    }
})