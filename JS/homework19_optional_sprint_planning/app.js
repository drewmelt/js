
const sumArr = arr => arr.reduce((a, c) => a + c)

const needDays = workTime => (tast, employee) => Math.ceil(tast / employee) * 24 / workTime

const haveDays = (start, end) => {
    let workedDays = 0
    for (let i = start; i < end; i += 1000*60*60*24) {
        let currentDay = new Date(i).getDay()
        if( currentDay !== 0 && currentDay !== 6){
            workedDays++
        }   
    }
    return workedDays
}


const main = (team, task, dl) => {
    const hours = 8
    const teamPoints = sumArr(team)
    const taskPoints = sumArr(task)

    const today = Date.now()
    const dedline =  new Date(dl).getTime()

    const timeInOffice = needDays(hours)
    const haveTime = haveDays(today,dedline)
    const needTime = timeInOffice(taskPoints, teamPoints)

    if(haveTime >= needTime){
        return `Все задачи будут успешно выполнены за ${haveTime - needTime} дней до наступления дедлайна!`
    }else{
        return `Команде разработчиков придется потратить дополнительно ${(needTime - haveTime) * hours} часов после дедлайна, чтобы выполнить все задачи в беклоге`
    }
}

const employeePoint = [1,2,4,4,5,5,7,8,4,6,8,9,5,3,3,6,8,6,4,3,5,7,9]

const taskPoint = [23,24,34,6,53,43,45,43,34,56,34,34,4,7,68,78,56]

console.log(main(employeePoint,taskPoint,'2021-02-20'));