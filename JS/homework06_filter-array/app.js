const filterBy = (array, type) => array.filter( elem => typeof elem !== type)

const arr = ['hello','world',NaN,'NaN','23',null,{a:1},false,Symbol('sd'),21]

console.log( 'null',    filterBy ( arr , 'null' ) )
console.log( 'NaN',     filterBy ( arr , 'NaN' ) )
console.log( 'number',  filterBy ( arr , 'number' ) )
console.log( 'string',  filterBy ( arr , 'string' ) )
console.log( 'bigint',  filterBy ( arr , 'bigint' ) )
console.log( 'symbol',  filterBy ( arr , 'symbol' ) )
console.log( 'boolean', filterBy ( arr , 'boolean' ) )
console.log( 'object',  filterBy ( arr , 'object' ) )