
const student  = {
    name: '',
    lastname: '',
    tabel: {},
}

const askSubject = object => (writeValue(object)) && askSubject(object)

const askData = (str , question = prompt(`input ${str}`)) => (question !== '') ? question : askData(str)

const writeValue = (obj, key, value) => {
    const userKey = key || askData('key') 
    if(!userKey) return false
    const userValue = value || askData(`value of ${userKey}:`)
    if(!userValue) return false

    isNaN(userValue) ? obj[userKey] = userValue : obj[userKey] = +userValue
    return true
}

const askValueOfKey = obj =>{
    for (const key in obj) {
        (key === 'tabel') ? askSubject(obj[key]) : writeValue(obj, key)
    }
    return obj
}

//return arr.length where mark < 4
const badMarksAmount = (obj, arr = [...Object.values(obj)]) => arr.length 
? arr.filter(item => item < 5).length 
: 'Err'

//show number of mark < 4 or return 'pass Exams'
const successExams = (obj, amount = badMarksAmount(obj)) => amount ? amount : 'Студент переведен на следующий курс'

const averageMarks = obj => {
    const markList = [...Object.values(obj)]
    if(markList.length){
        const sum = markList.reduce((a, c) => a + c);
        const average = sum / markList.length
    
        if(average > 7) {
            return 'Студенту назначена стипендия.'
        }
        return average
    }
    return 'no items'
}

const newStudent = askValueOfKey(student)

const ave = averageMarks(newStudent.tabel)
const success = successExams(newStudent.tabel)


console.log(ave);
console.log(success);
alert(ave)
alert(success)
