const form = document.querySelector('#form-js')
const btn = document.querySelector('.btn')
const error = document.querySelector('.error')

const setAtt = (elem, value) => elem.setAttribute("type", value)

btn.addEventListener('click', (e) => {
    e.preventDefault()
    const passList = [...form.querySelectorAll('.pass')]
    const corrPass = passList.every(elem => elem.value === passList[0].value)
    corrPass ? alert('Welcome') : error.textContent = 'Нужно ввести одинаковые значения'
})

form.addEventListener('click', (e) => {
    e.preventDefault()
    if (e.target.classList.contains('icon-password')) {
        const parent = e.target.closest('label')
        const input = parent.querySelector('.pass')
        if (input.type === 'password') {
            setAtt(input, 'text')
            e.target.classList.replace('fa-eye', 'fa-eye-slash')
        } else {
            setAtt(input, 'password')
            e.target.classList.replace('fa-eye-slash', 'fa-eye')
        }
    } else {
        const allPass = form.querySelectorAll('.input-wrapper')
        allPass.forEach(el => {
            setAtt(el.querySelector('.pass'), 'password')
            el.querySelector('.icon-password').classList.replace('fa-eye-slash', 'fa-eye')
        })
        error.textContent = ''
    }
})