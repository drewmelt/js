const slider = document.querySelector('.slides')
const prev = document.querySelector('#prev')
const next = document.querySelector('#next')
const stop = document.querySelector('#stop')
const start = document.querySelector('#start')


const initSlider = (parent, slideAmount) => {
    const start = [...parent.children].slice(0, slideAmount)
    const end = [...parent.children].slice(parent.children.length - slideAmount).reverse()
    for (let i = 0; i < slideAmount; i++) {
        parent.append(start[i].cloneNode(true))
        parent.prepend(end[i].cloneNode(true))
    }
}

const startSlider = (amountSlides, parent, width) => {
    const timerBlock = document.querySelector('.timer')
    let sec = 3000
    const showSec = () => {
        return setInterval(() => {
            sec = sec - 4
            let ans = parseInt(sec / 1000) + " sec " + sec % 1000 + " mili "
            timerBlock.textContent = ans
        }, 1)
    }
    let newInterval = showSec()
    const autoPlay = () => {
        const timer = setInterval(() => {
            move('ease .5s', 500)
            timerBlock.textContent = sec
            sec = 3000
        }, 3000)
        play = true
        return timer
    }
    const move = (anim, value) => {
        if (!canClick) return
        slider.style.transition = anim
        pos += -value
        slider.style.transform = `translateX(${pos}px)`
        canClick = false
    }
    initSlider(parent, amountSlides)

    let pos = -width
    let canClick = true
    let play = false
    const len = width * (parent.children.length - amountSlides)

    slider.style.transform = `translateX(${pos}px)`

    let timer = autoPlay()

    stop.addEventListener('click', () => {
        clearInterval(timer)
        play = false
        clearInterval(newInterval)
    })

    start.addEventListener('click', () => {
        if (!play) {
            newInterval = showSec()
            timerBlock.textContent = sec
            sec = 3000
            timer = autoPlay()
            play = true
        }
    })

    next.addEventListener('click', () => move('ease .5s', 500))
    prev.addEventListener('click', () => move('ease .5s', -500))

    slider.addEventListener('transitionend', () => {
        const absPos = Math.abs(pos)
        canClick = true
        if (absPos >= len) move('ease 0s', -2000)
        else if (absPos <= 0) move('ease 0s', 2000)
        canClick = true
    })
}

startSlider(1, slider, 500)