const tabs = document.querySelector('.tabs')
const tabsHeading = document.querySelectorAll('.tabs-title')
const tabsContent = document.querySelectorAll('.tabs-content li')

const tabsContentArr = [...tabsContent]
const tabsHeadingArr = [...tabsHeading]

const firstActive = (arr, className) => arr.findIndex(elem => elem.className.includes(className) )

const obj = Object.fromEntries(tabsContentArr.map((item, i) => [item.dataset.name , i]))

console.log(obj);

tabs.addEventListener('click', e => {
        if(![...e.target.children].length){    
            const indexActiveHeading = firstActive(tabsHeadingArr,'active')
            tabsHeading[indexActiveHeading].classList.remove('active')
            e.target.classList.add('active')
            const indexShowLi = firstActive(tabsContentArr,'unhidden')
            tabsContent[indexShowLi].classList.replace('unhidden','hidden')
            tabsContent[obj[e.target.dataset.heading]].classList.replace('hidden','unhidden')
       }
    }
)
