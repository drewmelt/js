const priceContainer = document.querySelector('.price-container-js')
const createPrice = document.querySelector('.price-input-js')
const btnRemove = document.querySelectorAll('.btn--remove')
const wrapper = document.querySelector('.wrapper')


const insertToHTML = (parent, data) => parent.insertAdjacentHTML('beforeend', data) || parent.lastElementChild
const removeFromHTML = (parent, elem) => parent.removeChild(elem)
const newPrice = price => `<p class="price"><span class="price__value">Текущая цена: ${price}</span><button class="btn--remove"></button></p>`
const error = `<p class="error">Please enter correct price</p>`


//Usage
createPrice.addEventListener('focus', (e) => {
    e.target.className = 'input'
    const errorElem = document.querySelector('.error')
    if (errorElem) removeFromHTML(wrapper, errorElem)
})

createPrice.addEventListener('blur', (e) => {
    const inputValue = e.target.value
    if (inputValue && inputValue >= 0) {
        insertToHTML(priceContainer, newPrice(inputValue))
        e.target.classList.add('input--success')
    } else {
        insertToHTML(wrapper, error)
        e.target.classList.add('input--danger')
    }
})

priceContainer.addEventListener('click', (e) => {
    if (e.target.className.includes('btn--remove')) {
        removeFromHTML(e.target.parentElement.closest('div'), e.target.parentElement)
        createPrice.value = ''
        createPrice.className = 'input'
    }
})