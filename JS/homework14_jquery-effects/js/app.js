$('.anchor-wrapper').on('click', 'a', (e) => {
    const href = $(e.target).attr('href')
    const position = $(href).offset().top - $('.anchor-wrapper').height()
    $('html').animate({ scrollTop: position }, 800)
})

$("#most-popular-clients").on('click', () => {
    $("#most-popular-clients .brand-list").slideToggle(400)
})

$(window).scroll(() => {
    if ($(window).scrollTop() > $(window).height()) {
        $('.to-top').show()
    } else {
        $('.to-top').hide()
    }
})

$('.to-top').on('click', () => {
    $('html').animate({ scrollTop: 0 }, 800)
})