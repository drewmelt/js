const sities = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", ["Sity",["Sity",["Sity",["Sity"]]]], "Lviv", "Dnieper"];

const listWrapper = document.querySelector('.list__wrapper')
const container = document.querySelector('.container')
const body = document.querySelector('.body')

const insertToHTML = (place, data) => place.insertAdjacentHTML('beforeend', data) || place.lastElementChild

const createList = arr => ul(arr.map(item => Array.isArray(item) ? createList(item) : li(item)).join(''))

const createTag = (tag, className) => (...value) => `<${tag} class="${className}">${value.join('')}</${tag}>`

const changeTextContent = (elem, value = '') => elem.textContent = value


const createInterval = (elem, i) => {
    changeTextContent(elem,i)
    const interval = setInterval(() => {
            changeTextContent(elem,--i)
            if(i < 0){
                clearInterval(interval)
                changeTextContent(body)
            }
    }, 1000)
}

const li = createTag('li','list__item')
const ul = createTag('ul','list')
const div = createTag('div','timer')

const listURL = insertToHTML(listWrapper, createList(sities))
const timerURL = insertToHTML(container, div())
createInterval(timerURL,10)





