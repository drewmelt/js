const createCircle = document.querySelector('.create-circle-js')
const container = document.querySelector('.container')

const insertToHTML = (place, data) => place.insertAdjacentHTML('beforeend', data) || place.lastElementChild
const removeFromHTML = (parent, elem) => parent.removeChild(elem)
const сircle = () => `<div class="circle"></div>`

const changeBG = elem => elem.style.background = randRGB()
const changeSize = (elem, size) => (elem.style.width = size + 'px', elem.style.height = size + 'px')

const randNum = range => Math.round(Math.random() * range) 
const randRGB = () => `rgb(${randNum(255)},${randNum(255)},${randNum(255)})`

const removeCircle = e => {
    if(e.target.className.includes('circle'))
        removeFromHTML(container, e.target)
}

const createCircles = () => {
    const radius = +prompt('Radius in pixels:')
    for (let i = 0; i < 100; i++) {
        let newCircle = insertToHTML(container, сircle())
        changeBG(newCircle)
        changeSize(newCircle, radius)
    }
}

createCircle.addEventListener('click', createCircles)
container.addEventListener('click', removeCircle)
