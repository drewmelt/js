const obj = {
    age: 19,
    name: 'andrew',
    skills: {
        lang: ['urk','rus','eng','fra','by'],
        proglang: {
            js: {
                name: null,
                skillPoint: 3,
            },
            go: {
                name: 'golang',
                skillPoint: 1,
            },
            c: {
                list: [{
                    name: 'c',
                    skillPoint: 10,
                },
                {
                    name: 'c#',
                    skillPoint: 5,
                },
                {
                    name: 'c++',
                    skillPoint: 6,
                },
            ]},
            java: {
                name: 'java',
                skillPoint: 3,
            },
            reactJs: {
                name: 'ReactJS',
                skillPoint: 3,
            },
        }
    }
}


const copyObject = (obj) => {
    const newObj = {}
    for (const key in obj) {
        if((typeof obj[key] === 'object' && obj[key] !== null) && !Array.isArray(obj[key])){
            newObj[key] = copyObject(obj[key])
        }else if(Array.isArray(obj[key])){
            newObj[key] = []
            for (const item of obj[key]) {
                if(typeof item !== 'object' || item === null){
                    newObj[key].push(item)
                }else{
                    newObj[key] = copyObject(item)
                }
            }
        }else{
            newObj[key] = obj[key]
        }
    }
    return newObj
}

const newObj = copyObject(obj)

console.log(obj);
console.log(newObj.skills.proglang);

console.log(obj === newObj);