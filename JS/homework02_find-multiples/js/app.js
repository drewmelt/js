const askUserNumber = (str, number = +prompt(`Input ${str}`)) => ((!isNaN(number) && number) || number === 0) ? number : askUserNumber(str)

const primeError = 'not prime number'
const noNumbers = 'Sorry, no numbers'

function firstTask(){
    const userNumber = askUserNumber('a random number')
    
    if ( userNumber < 0 ) return noNumbers

    for (let i = 0; i < userNumber; i += 5)
        console.log(i)

    alert(`isInteger ${Number.isInteger(userNumber)}`)
}


firstTask()
