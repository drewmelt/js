function isPrime (number) {
    const primeNumbers = [2,3]
    
    if ( number <= 1 ) return false

    for (let i = 0; i < primeNumbers.length; i++)
        if( primeNumbers[i] === number ) return true

    const primePattern = (number * number - 1) % 24

    if ( primePattern === 0 ){
        for (let i = 2; i <= Math.sqrt( number ); i++)
            if ( number % i === 0 ) return false 
        return true

    } else { return false }
}

function secondTask(){
    
    const startNumber = askUserNumber('startNumber')
    const endNumber = askUserNumber('endNumber')
    
    if ( startNumber > endNumber) return alert( noNumbers )

    for (let i = startNumber; i <= endNumber; i++) {
        console.log(`${i}: ${isPrime(i)}`);
    }

}

//2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199

secondTask()