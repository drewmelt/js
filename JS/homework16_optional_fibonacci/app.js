
const fib = (f1, f2, n = +prompt('input index get value')) => {
    if(n === 0) return f1
    if(n < 0)   return fib(f2, f1 - f2, n + 1)
    if(n > 0)   return fib(f2, f2 + f1, n - 1)
}
alert(fib(1,1));