
const askUserName = (name = prompt("Input your name")) => name ? name : askUserName()

const askUserAge = (age = +prompt("Input your age")) => ( !isNaN( age ) && age ) ? age : askUserAge()


function askUserData() {

    const cancelEnter = "You are not allowed to visit this website"
    const askToEnter = "Are you sure you want to continue?"
    
    const userName = askUserName()
    const userAge = askUserAge()
    
    if ( userAge < 18 ){
        alert( cancelEnter )
    }else if( userAge >= 18 && userAge <= 22 ){
        const confirmToEnter = confirm( askToEnter )
        if(confirmToEnter){
            alert(`Welcome ${userName}`)
        }else{
            alert( cancelEnter )
        }
    }else{
        alert(`Welcome ${userName}`)
    }
}

askUserData()
