const askUserNumber = (str, number = +prompt(`Input ${str}`)) => ( !isNaN( number ) && number ) ? number : askUserNumber(str)

const askUserOperation = (str, operation = prompt(`Input ${str}`)) => operation ? operation : askUserOperation(str)

function calculateTwoNums (a, b, operation) {
    switch (operation) {
        case "+": return a + b
        case "-": return a - b
        case "*": return a * b
        case "/": return a / b
        default: return 'dont have operation like: ' + operation
    }
}

const firstNumber = askUserNumber('first number')
const secondNumber = askUserNumber('second number')
const operation = askUserOperation('operation (+,-,*,/)')

const answer = calculateTwoNums(firstNumber,secondNumber,operation)

alert(answer)