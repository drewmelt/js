const vehicles = {
    description: [1,2,3,4,5,6,7],
    contentType: {name: [1,2,3,'en_US',5,6,7,8]},
    locales: {
        description: [1,2,23,4,5,7],
        name: [1,2,'en_US',4,5,'Toyota','Toyota',8],
    }
}

//find arr
const findArr = (obj, url) => (url.length > 1) ? findArr(obj[url[0]],url.slice(1)) : obj[url[0]]

const deleteEmpty = obj => {
    for (const key in obj) 
        if (!obj[key].length) 
            delete obj[key]
    return obj
}

function filterCollection(obj, search, flag, ...urls) {
    const ans = Object.create(null)
    const words = search.split(' ')
    const arrUrl = str => findArr(obj, str.split('.'));
    
    //find all
    if(flag){
        urls.forEach(c => ans[c] = arrUrl(c)
        .map(elem => words
            .find(word => elem === word))
            .filter(e => e !== undefined))
    }
    //find first
    for (const url of urls) {
        ans[url] = []
        let array = arrUrl(url)
        for (let i = 0; i < array.length; i++) {
            if(search.includes(array[i])){
                ans[url].push(array[i])
                return deleteEmpty(ans)
            }
        }
    }
    return deleteEmpty(ans)
}

const sorted = filterCollection(vehicles, 'Toyota en_US', true, 'description', 'contentType.name', 'locales.name', 'locales.description')
// console.log(deleteEmpty(sorted));
console.log(sorted);
