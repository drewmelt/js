
function createUserOptional(firstName, lastName){
    return Object.create({},
    {
        firstname: {
            value: firstName.toLowerCase(),
            enumerable: true,
            writable: false,
            configurable: true

        },
        lastname: {
            value: lastName.toLowerCase(),
            enumerable: true,
            writable: false,
            configurable: true
        },
        getLogin: {
            value: function(){
                return `${this.firstname[0]}${this.lastname}`.toLowerCase()
            }
        },
        setFirstName: {
            value: function(data){
            Object.defineProperty(this, 'firstname', {value: data})
        }},
        setLastName: {
            value: function(data){
            Object.defineProperty(this, 'lastname', {value: data})
        }},
    }
    )
}

const userFirstName = askUserData('firstname')
const userLastName = askUserData('lastname')

const newUserOptional = createUserOptional(userFirstName,userLastName)

console.log(newUser);