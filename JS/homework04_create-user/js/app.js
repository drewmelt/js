const askUserData = (str, inputData = prompt(`Input ${str}`)) => inputData ? inputData : askUserData(str)

const createUser = (firstName, lastName) => {
    return {
        firstname: firstName,
        lastname: lastName,
        getLogin: function(){
            return `${this.firstname[0]}${this.lastname}`.toLowerCase()
        }
    }
}
const createFirstName = askUserData('firstname')
const createLastName = askUserData('lastname')

const newUser = createUser(createFirstName,createLastName)

console.log(newUser.getLogin());