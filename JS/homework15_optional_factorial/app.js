const askNumber = (num = +prompt('Input integer')) => Number.isInteger(num) ? factorial(num) : askNumber() 

//при использовании рекурсии возможна ошибка переполнения стека
const factorial = num => ( num > 0 ) ? num * factorial(num - 1) : 1

// const factorial = num => {
//     let ans = 1
//     for (let i = 1; i <= num; i++) {
//         ans = ans * i
//     }
//     return ans
// }

alert(askNumber())