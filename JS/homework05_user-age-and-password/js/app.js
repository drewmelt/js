const askUserData = (str, cb = data => data !== '', inputData = prompt(`Input ${str}`)) => cb(inputData) ? inputData : askUserData(str,cb)

function createUser(firstName, lastName, Birthday){
    return {
        firstname: firstName,
        lastname: lastName,
        birthday: {
                day: Birthday.day,
                mounth: Birthday.mounth,
                year: Birthday.year
        },
        getLogin: function(){
            return `${this.firstname[0]}${this.lastname}`.toLowerCase()
        },
        getAge: function() {
            return new Date().getFullYear() - this.birthday.year
        },
        getPassword: function() {
            return `${this.birthday.year}${this.firstname[0].toUpperCase()}${this.lastname.toLowerCase()}`
        }
    }
}

const isNumber = value => (!isNaN( value ) && value !== '')

const userFirstName = askUserData('firstname')
const userLastName = askUserData('lastname')

const userBirthdayDay = +askUserData('birthday day', isNumber)
const userBirthdayMounth = +askUserData('birthday mounth', isNumber)
const userBirthdayYear = +askUserData('birthday year', isNumber)

const userBirthday = {
    day: userBirthdayDay,
    mounth: userBirthdayMounth,
    year: userBirthdayYear,
}

const newUser = createUser(userFirstName, userLastName, userBirthday)

console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

