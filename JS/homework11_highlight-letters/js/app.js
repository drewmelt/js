const btnWrapper = document.querySelector('.btn-wrapper')
const btns = document.querySelectorAll('.btn')

const firstActive = (arr, className) => arr.findIndex(elem => elem.className.includes(className))

const objBtn = [...btns].reduce((obj, item, index) => (obj[item.dataset.key] = index, obj), {})

const highlightLetters = e => {
    const index = objBtn[e.key]
    if (index !== undefined) {
        const findActive = firstActive([...btns], 'active')
        if (findActive >= 0) btns[findActive].classList.remove('active')
        btns[index].classList.add('active')
    }
}


document.addEventListener('keydown', highlightLetters)