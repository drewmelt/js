const body = document.body
const swicthTheme = document.querySelector('.switch-theme')
const switchBtn = document.querySelector('.switch-btn')

const getTheme = () => localStorage.getItem('theme')

const setTheme = themeName => {
    localStorage.setItem('theme', themeName)
    return getTheme()
}
const initTheme = () => {
    const currTheme = getTheme()
    body.className = currTheme ? currTheme : setTheme('theme--light')

    if (currTheme === 'theme--dark')
        switchBtn.classList.toggle('change--theme')
}

initTheme()

swicthTheme.addEventListener('click', () => {
    switchBtn.classList.toggle('change--theme')

    body.className = body.classList.contains('theme--light') ?
        setTheme('theme--dark') :
        setTheme('theme--light')

})